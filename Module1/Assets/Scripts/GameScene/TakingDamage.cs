﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;

public class TakingDamage : MonoBehaviourPunCallbacks
{

    [SerializeField]
    Image healthbar;

    private float startHealth = 100;
    public float currentHealth;

    void Start()
    {
        currentHealth = startHealth;
        healthbar.fillAmount = currentHealth / startHealth;
    }

    [PunRPC]
    public void TakeDamage(int damage) {
        currentHealth -= damage;
        Debug.Log(currentHealth);

        healthbar.fillAmount = currentHealth / startHealth;

        if(currentHealth <= 0) {
            Die();
        }
    }

    private void Die() {

        if(photonView.IsMine)
        GameManager.insatance.LeaveRoom();
        
    }
}
