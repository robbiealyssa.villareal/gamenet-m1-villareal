﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviourPunCallbacks
{
    [SerializeField]
    private GameObject playerPrefab;

    // Singleton
    public static GameManager insatance;

    private void Awake() {

        if(insatance != null){

            Destroy(this.gameObject);

        } else {

            insatance = this;
            
        }

    }

    void Start()
    {
        if(PhotonNetwork.IsConnected) {
            if(playerPrefab != null) {
                int xRndPoint = Random.Range(-20, 20);
                int zRndPoint = Random.Range(-20, 20);

                PhotonNetwork.Instantiate(playerPrefab.name, new Vector3(xRndPoint, 0, zRndPoint), Quaternion.identity);
            }
        }
    }

    // Callbacks 
    public override void OnJoinedRoom()
    {
        Debug.Log(PhotonNetwork.NickName + " Has Joined the Room! ");
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        Debug.Log(PhotonNetwork.NickName + " Has joined the Room!" + PhotonNetwork.CurrentRoom.Name);
        Debug.Log("Room has now " + PhotonNetwork.CurrentRoom.PlayerCount + " /20 players");

    }

    public override void OnLeftRoom()
    {
        // When player leaves the room go back to Lobby
        SceneManager.LoadScene("GameLauncherScene");
    }

    public void LeaveRoom() {
        PhotonNetwork.LeaveRoom();
    }

}
