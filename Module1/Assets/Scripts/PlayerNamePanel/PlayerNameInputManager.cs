using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class PlayerNameInputManager : MonoBehaviour
{
    public void SetPlayerName(string playerName){

        // Check if empty
        if(string.IsNullOrEmpty(playerName)) {
            Debug.LogWarning("PLayer name is empty!");
            return;
        }

        PhotonNetwork.NickName = playerName;
    }

}
