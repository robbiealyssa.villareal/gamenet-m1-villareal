using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;  // Handles all Rooms and Lobbies

public class LaunchManager : MonoBehaviourPunCallbacks
{
    public GameObject EnterGamePanel;
    public GameObject ConnectionStatusPanel; // Set active when conencted 
    public GameObject LobbyPanel;

    void Awake(){
        // *Will load for every player/client that joins the scene of master client
        PhotonNetwork.AutomaticallySyncScene = true;
    }
    void Start()
    {
        EnterGamePanel.SetActive(true);
        ConnectionStatusPanel.SetActive(false);
        LobbyPanel.SetActive(false);
    }

    void Update()
    {
        
    }

    // Callbacks
    // Is called when connected to Photon Server ; To Determine Status Connection
    public override void OnConnectedToMaster()
    {
        Debug.Log(PhotonNetwork.NickName + " Connected to Photon Servers");
        ConnectionStatusPanel.SetActive(false);
        LobbyPanel.SetActive(true);
    }

    // Check if connected to Internet 
    public override void OnConnected()
    {
        Debug.Log("Connected to Internet");
    }

    // Called when no match found
    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        Debug.LogWarning(message);
        CreateAndJoinRoom();
    }

    // Connect to Photon Server using the nickname
    public void ConnectToPhotonServer()
    {
        // Check if not connected 
        if(!PhotonNetwork.IsConnected) {
            PhotonNetwork.ConnectUsingSettings();
            ConnectionStatusPanel.SetActive(true);
            EnterGamePanel.SetActive(false);
        }
    }

    public void JoinRandomRoom() {
        PhotonNetwork.JoinRandomRoom();
    }


    private void CreateAndJoinRoom() {

        string randomRoomName = "Room " + Random.Range(0, 10000);

        RoomOptions roomOptions = new RoomOptions();
        roomOptions.IsOpen = true;
        roomOptions.IsVisible = true;
        roomOptions.MaxPlayers = 20;

        PhotonNetwork.CreateRoom(randomRoomName, roomOptions);
    }


    // Check if joined 
    public override void OnJoinedRoom()
    {
        Debug.Log(PhotonNetwork.NickName + " Has entered " +  PhotonNetwork.CurrentRoom.Name);
        PhotonNetwork.LoadLevel("GameScene");
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        Debug.Log(newPlayer.NickName + " Has entered " + PhotonNetwork.CurrentRoom.Name + " Room has now " + 
                PhotonNetwork.CurrentRoom.PlayerCount + " players.");
    }

}
