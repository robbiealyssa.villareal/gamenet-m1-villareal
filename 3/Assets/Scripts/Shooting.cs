﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;

public class Shooting : MonoBehaviourPunCallbacks
{
    public GameObject hitEffectPrefab;

    [Header("HP Stuff")]
    public float startHealth = 100;
    private float health;
    public Image healthbar;

    public Camera camera;
//
    public float killCount;
    public bool isDead;
    public float delay = 5f;

    void Start()
    {
        health = startHealth;
        healthbar.fillAmount = health / startHealth;

        isDead = false;
    }

    
    void Update(){
        if(Input.GetMouseButton(0))
        {
            fire();
        }
    }


    public void fire()
    {
        RaycastHit hit;
        Ray ray = camera.ViewportPointToRay(new Vector3(0.5f, 0.5f));

        if(Physics.Raycast(ray, out hit, 200))
        {
            Debug.Log(hit.collider.gameObject.name);

            photonView.RPC("CreateHitEffects", RpcTarget.All, hit.point); // when player fires, all players can see direction 
        }
        
        if(hit.collider.gameObject.CompareTag("Player") && !hit.collider.gameObject.GetComponent<PhotonView>().IsMine)
        {
            hit.collider.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, 10);
        }

    }

    [PunRPC]
    public void TakeDamage(int damage, PhotonMessageInfo info)
    {
        this.health -= damage;
        this.healthbar.fillAmount = health / startHealth;

        if(health <= 0 && !isDead)
        {
            health = 0; 

            isDead = true;

            // UI who killed who    
            Debug.Log(info.Sender.NickName + " killed " + info.photonView.Owner.NickName); 
            //GameObject whoKilledText = GameObject.Find("Who Killed Text");
            //whoKilledText.GetComponent<Text>().text = info.Sender.NickName + " killed " + info.photonView.Owner.NickName;

            //Die();
            photonView.RPC("Die",RpcTarget.AllBuffered);

        }

    }

    [PunRPC]
    public void CreateHitEffects(Vector3 position)
    {
        GameObject hitEffectGameObject = Instantiate(hitEffectPrefab, position, Quaternion.identity);
        Destroy(hitEffectGameObject, .2f);
    }

     [PunRPC]
    public void Die(PhotonMessageInfo info) 
    {
        if(photonView.IsMine)
        {
            isDead = true;
            Debug.Log(info.Sender.NickName + " Won!");
        }

        //if(killCount >= 3) //change
        //{
          //  Debug.Log(info.Sender.NickName + " Won!");
           // StartCoroutine(ShowWhoWon(info, delay));

        //}

    }

    [PunRPC]
    IEnumerator ShowWhoWon(PhotonMessageInfo info, float delay)
    {
        
        GameObject showWhoWonText = GameObject.Find("Show Who Won Text");
        showWhoWonText.GetComponent<Text>().text = info.Sender.NickName + " Won!";

        yield return new WaitForSeconds(delay);

        PhotonNetwork.LoadLevel("LobbyScene");
    }



}
