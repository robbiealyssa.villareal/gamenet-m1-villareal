﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;
using UnityEngine.UI;

public class LapController : MonoBehaviourPunCallbacks
{

    public List<GameObject> lapTriggers = new List<GameObject>();

    public enum RaiseEventCode
    {
        WhoFinishedEventCode = 0
    }

    private int finishOrder = 0;

    // After game finish: OnEnable & OnDisable 
    //Add listners to all listeners for our events 
    private void OnEnable()
    {
        // add OnEvent as a callback for our listeners 
        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
    }
    private void OnDisable()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;
    }

    //add callback when an event is called 
    void OnEvent(EventData photonEvent)
    { 
        //checking if the event that has been sent was for "Who finished event"
        if(photonEvent.Code == (byte)RaiseEventCode.WhoFinishedEventCode)
        {
            // need to retrieve the data that have passed for our event 
            object[] data = (object[]) photonEvent.CustomData;

            string nickNameOfFinishPlayer = (string)data[0];
            finishOrder = (int)data[1];

            //get view ID
            int viewId = (int)data[2];

            Debug.Log(nickNameOfFinishPlayer + " (#" + finishOrder + ") ");

            GameObject orderUiText = RacingGameManager.instance.finisherTextUi[finishOrder - 1];
            orderUiText.SetActive(true);

            if(viewId == photonView.ViewID) //this is you 
            {
                orderUiText.GetComponent<Text>().text = "(#" + finishOrder + ") " + nickNameOfFinishPlayer + "(YOU)";
                orderUiText.GetComponent<Text>().color = Color.red;
            }
            else
            {
                orderUiText.GetComponent<Text>().text = "(#" + finishOrder + ") " + nickNameOfFinishPlayer;
            }

        }
    }

    // Start is called before the first frame update
    void Start()
    {
        foreach (GameObject go in RacingGameManager.instance.lapTriggers) //go - gameObject
        {
            lapTriggers.Add(go);
        } 
    }

    private void OnTriggerEnter(Collider col)
    {
        if(lapTriggers.Contains(col.gameObject))
        {
            int indexOfTrigger = lapTriggers.IndexOf(col.gameObject);

            lapTriggers[indexOfTrigger].SetActive(false);
        }

        if(col.gameObject.tag == "FinishTrigger")
        {   
            //trigger game finish 
            GameFinish();
        }
    }

    public void GameFinish()
    {
        GetComponent<PlayerSetup>().camera.transform.parent = null;
        GetComponent<VehicleMovement>().enabled = false;

        finishOrder ++;
        string nickname = photonView.Owner.NickName;
        int viewId = photonView.ViewID; // can see ranking inside our canvas

        //event data
        object[] data = new object[] { nickname, finishOrder, viewId };

        RaiseEventOptions raiseEventOptions = new RaiseEventOptions
        {
            Receivers = ReceiverGroup.All,
            CachingOption = EventCaching.AddToRoomCache
        };

        SendOptions sendOption = new SendOptions
        {
            Reliability = false
        };

        PhotonNetwork.RaiseEvent((byte) RaiseEventCode.WhoFinishedEventCode, data, raiseEventOptions, sendOption);
    }
}
