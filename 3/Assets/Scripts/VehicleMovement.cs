﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VehicleMovement : MonoBehaviour
{

    public float speed = 20; 
    public float rotSpeed = 200;
    public float currentSpeed = 0;

    public bool isControlledEnabled;

    void Start()
    {
        isControlledEnabled = false;
    }
    
    // Update is called once per frame
    void LateUpdate()
    {
        if(isControlledEnabled)
        {
            float translation = Input.GetAxis("Vertical") * speed * Time.deltaTime;
            float rot = Input.GetAxis("Horizontal")* rotSpeed * Time.deltaTime;

            transform.Translate(0, 0, -translation);
            currentSpeed = translation;

            transform.Rotate(0, rot, 0);
        }
    }
}
