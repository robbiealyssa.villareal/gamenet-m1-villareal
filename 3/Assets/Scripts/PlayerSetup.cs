﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Photon.Pun;

public class PlayerSetup : MonoBehaviourPunCallbacks
{

    public Camera camera;
    //public GameObject playerUiPrefab;
    


    // Start is called before the first frame update
    void Start()
    {
        this.camera = transform.Find("Camera").GetComponent<Camera>();

        if(PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("rc"))
        {
            GetComponent<VehicleMovement>().enabled = photonView.IsMine;
            GetComponent<LapController>().enabled = photonView.IsMine; //disable if its not ours
            camera.enabled = photonView.IsMine;
        }   
        else if(PhotonNetwork.CurrentRoom.CustomProperties.ContainsValue("dr")) 
        {
            GetComponent<VehicleMovement>().enabled = photonView.IsMine;
            camera.enabled = photonView.IsMine;
        }

        //if(photonView.IsMine)
        //{
          //  GameObject playerUi = Instantiate(playerUiPrefab);
            
        //}
        
    }

    
}
