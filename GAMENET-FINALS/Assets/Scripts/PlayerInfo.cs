﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;

public class PlayerInfo : MonoBehaviourPunCallbacks
{
    //Health 
    //Collisions ?
    //Events 
    //RPC

    public List<GameObject> finishTriggers = new List<GameObject>();

    [Header ("HP Stuff")]
    public float startHealth = 100;
    private float health;
    public bool isDead;
    public Image healthbar; 

    private int finishOrder = 0;
    private int diedOrder = 0;

    public static PlayerInfo instance;

    public enum RaiseEventCode
    {
        WhoFinishedEventCode = 0,
        WhoDiedInsideEventCode = 1
    }

    // After GameFinish(): OnEnable & OnDisable 
    //Add listners to all listeners for our events 
    private void OnEnable()
    {
        // add OnEvent as a callback for our listeners 
        PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
    }
    private void OnDisable()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;
    }

    void OnEvent(EventData photonEvent)
    {
        if(photonEvent.Code == (byte)RaiseEventCode.WhoFinishedEventCode)
        {
            object[] data = (object[]) photonEvent.CustomData;

            string nicknameOfFinishPlayer = (string)data[0];
            finishOrder = (int)data[1];
            int viewId = (int)data[2];

            //Debug.Log(nicknameOfFinishPlayer + " (#" + finishOrder + ") ");

            GameObject orderUiText = GameManager.instance.finisherTextUi[finishOrder - 1];
            orderUiText.SetActive(true);

            if(viewId == photonView.ViewID)
            {
                orderUiText.GetComponent<Text>().text = "(#" + finishOrder + ") " + nicknameOfFinishPlayer;
                orderUiText.GetComponent<Text>().color = Color.white;
            }
            else{
                orderUiText.GetComponent<Text>().text = "(#" + finishOrder + ") " + nicknameOfFinishPlayer + "(YOU)";
                orderUiText.GetComponent<Text>().color = Color.green;
            }
        }


        if(photonEvent.Code == (byte)RaiseEventCode.WhoDiedInsideEventCode)
        {
            object[] data = (object[]) photonEvent.CustomData;

            string nicknameOfPlayerDied = (string)data[0];
            diedOrder = (int)data[1];
            int viewId = (int)data[2];

            Debug.Log(nicknameOfPlayerDied +  " (#" + diedOrder + ") ");

            GameObject deadOrderUiText = GameManager.instance.deadTextUi[diedOrder - 1];
            deadOrderUiText.SetActive(true);

            deadOrderUiText.GetComponent<Text>().text = "(#" + diedOrder + ") " + nicknameOfPlayerDied;
            deadOrderUiText.GetComponent<Text>().color = Color.red;

        }


    }

    void Start()
    {
        health = startHealth;
        healthbar.fillAmount = health/startHealth;
        isDead = false;
        
    }

    void Update() { 
    }

    public void OnCollisionEnter(Collision col)
    {
        //WHEN PLAYER COLLIDED WITH ENEMY = TAKE DAMAGE
        if(col.gameObject.tag == "Enemy" && photonView.IsMine)
        {
            Debug.Log("Collided with Enemy");
            this.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, 20);
        }

        if(col.gameObject.tag == "FinishLine" && photonView.IsMine)
        {
            Debug.Log("Game Finish - reached outside");
            //
            photonView.RPC("PlayerEscaped", RpcTarget.AllBuffered);
            //GameFinish();
        }
    }

    [PunRPC]
    public void TakeDamage(int damage, PhotonMessageInfo info)
    {
        this.health -= damage;
        this.healthbar.fillAmount = health / startHealth;

        if(health <= 0 && !isDead)
        {
            health = 0; 

            isDead = true;

            //photon pun function
            //photonView.RPC("Die", RpcTarget.AllBuffered);

            if(photonView.IsMine)
            {
                Debug.Log(info.photonView.Owner.NickName + " got killed! - GAMEOVER!");
                GameObject whoDiedText = GameManager.instance.playerDiedTextUi;
                whoDiedText.GetComponent<Text>().text = "YOU GOT KILLED!";
                whoDiedText.SetActive(true);
                
                //STOP THE SCREEN ?
                //or
                // StartCoroutine(showGameOverScreen);
            }

            PlayerDied();
        }
    }

    [PunRPC]
    public void Die(PhotonMessageInfo info)
    {
        if(photonView.IsMine)
        {
            Debug.Log(info.photonView.Owner.NickName + " got killed! - GAMEOVER!");
            GameObject whoDiedText = GameManager.instance.playerDiedTextUi;
            whoDiedText.GetComponent<Text>().text = "YOU GOT KILLED!";
            whoDiedText.SetActive(true);
            
            //STOP THE SCREEN ?
            //or
            // StartCoroutine(showGameOverScreen);

            PlayerDied();
        }
    } 

    [PunRPC]
    public void PlayerEscaped(PhotonMessageInfo info)
    {
        if(photonView.IsMine)
        {
            //Debug.Log("YOU ESCAPED!!");

            //SHOW " YOU WINNER " TEXT (RPC?) or StartCouroutine???
            Debug.Log(info.photonView.Owner.NickName + " Escaped! - you Win!");
            GameObject whoWonText = GameManager.instance.winnerTextUi;
            whoWonText.GetComponent<Text>().text = "You Won!";
            whoWonText.SetActive(true);

            GameFinish();
        }
    }


    public void GameFinish()  // PLAYER REACHED THE FINISH LINE 
    {
        GetComponent<PlayerMovement>().enabled = false; 

        finishOrder++;
        string nicknameW = photonView.Owner.NickName;
        int viewId = photonView.ViewID;

        object[] data = new object[] {nicknameW, finishOrder, viewId};

        RaiseEventOptions raiseEventOptions = new RaiseEventOptions
        {
            Receivers = ReceiverGroup.All,
            CachingOption = EventCaching.AddToRoomCache
        };

        SendOptions sendOptions = new SendOptions
        {
            Reliability = false
        };
    
        PhotonNetwork.RaiseEvent((byte) RaiseEventCode.WhoFinishedEventCode, data, raiseEventOptions, sendOptions);
    
    }

    public void PlayerDied() // PLAYER DIED INSIDE THE MAZE
    {
        GetComponent<PlayerMovement>().enabled = false;

        diedOrder++;
        string nicknameL = photonView.Owner.NickName;
        int viewId = photonView.ViewID;

        object[] data = new object[] {nicknameL, diedOrder, viewId};

        RaiseEventOptions raiseEventOptions = new RaiseEventOptions
        {
            Receivers = ReceiverGroup.All,
            CachingOption = EventCaching.AddToRoomCache
        };

        SendOptions sendOptions = new SendOptions
        {
            Reliability = false
        };

        PhotonNetwork.RaiseEvent((byte) RaiseEventCode.WhoDiedInsideEventCode, data, raiseEventOptions, sendOptions);

    }


}
