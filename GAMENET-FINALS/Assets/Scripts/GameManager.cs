﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;

public class GameManager : MonoBehaviourPunCallbacks
{
    public GameObject playerPrefab;

    //public GameObject enemyPrefab; // delete
    public GameObject[] enemyPrefabs;

    //public Transform enemySpawnPoint; //delete
    public Transform[] enemySpawnPoints;
    public Transform[] spawnPoints;

    // UI TEXTS
    public GameObject[] finisherTextUi;
    public GameObject[] deadTextUi;
    public GameObject winnerTextUi;
    public GameObject playerDiedTextUi;

    public static GameManager instance = null;

    //public Text timerText;

    public List<GameObject> finishTriggers = new List<GameObject>();

    void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else if(instance != this)
        {
            Destroy(gameObject);
        } 
        
        DontDestroyOnLoad(gameObject);
    
    }

    void Start()
    {
        if(PhotonNetwork.IsConnectedAndReady)
        {
            // PLAYER INSTANTIATE: POSITION/LOCATION
            int rndPos = Random.Range(0, spawnPoints.Length);

            PhotonNetwork.Instantiate(playerPrefab.name, spawnPoints[rndPos].transform.position, spawnPoints[rndPos].transform.rotation);

            //ENEMY INSTANTIATE: POSITION/LOCATION ; ENEMY TYPE
           
            int rndPosEnemy = Random.Range(0, enemySpawnPoints.Length);
            int rndEnemy = Random.Range(0, enemyPrefabs.Length);

            for(int i = 0; i <enemySpawnPoints.Length; i++)
            {
                GameObject enem = PhotonNetwork.Instantiate(enemyPrefabs[UnityEngine.Random.Range(0, enemyPrefabs.Length)].name,
                                                            enemySpawnPoints[i].transform.position,
                                                            enemySpawnPoints[i].transform.rotation);
                
                enem.GetComponent<EnemyAi>().SetTarget(playerPrefab.transform);
            }
            
        }    

        foreach(GameObject go in finisherTextUi)
        {
            go.SetActive(false);
        }

        foreach(GameObject go in deadTextUi)
        {
            go.SetActive(false);
        }
        
        winnerTextUi.SetActive(false);
        playerDiedTextUi.SetActive(false);

    }

    void Update()
    {}

    [PunRPC]
    public void spawnEnemies(int rndPosEnemy, int rndEnemy,  PhotonMessageInfo info)
    {
        //int RndPosEnemy = Random.Range(0, enemySpawnPoints.Length);
        //int RndEnemy = Random.Range(0, enemyPrefabs.Length);

        //RndPosEnemy = rndPosEnemy;
        //RndEnemy = rndEnemy;
        GameObject enemGo = PhotonNetwork.Instantiate(enemyPrefabs[rndEnemy].name,
                                        enemySpawnPoints[rndPosEnemy].transform.position,
                                        enemySpawnPoints[rndPosEnemy].transform.rotation);

        enemGo.GetComponent<EnemyAi>().SetTarget(playerPrefab.transform);
    }
}
