﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Photon.Pun;

public class PlayerSetup : MonoBehaviourPunCallbacks
{
    [SerializeField]
    TextMeshProUGUI playerNameText;

    public GameObject player;
    public PlayerMovement playerMovement;
    void Start()
    {
        playerMovement = this.GetComponent<PlayerMovement>();

        if(photonView.IsMine)
        {
            //FIX CONTROLLER!!!!
            GetComponent<PlayerMovement>().enabled = photonView.IsMine;
            playerMovement.enabled = true;
            //playerMovement.playerCamera.enabled = photonView.IsMine;
            playerMovement.playerCamera.enabled = true;
        }
        else
        {
            playerMovement.enabled = false;
            playerMovement.playerCamera.enabled = false;

        }

        playerNameText.text = photonView.Owner.NickName;
    }

    void Update()
    {
        
    }
}
