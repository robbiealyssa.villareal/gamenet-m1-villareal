﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public Camera playerCamera = null;

    private Rigidbody rigidBody = null;
    private int turn = 10;
    private bool isFloorTouched;

    void Start()
    {
        rigidBody = GetComponent<Rigidbody>();
        isFloorTouched = false;
    }
    void Update() {}

    void FixedUpdate()
    {
        if(rigidBody != null)
        {
            //Controlls
            if(Input.GetButton("Horizontal"))
            {
                rigidBody.AddTorque(Vector3.back*Input.GetAxis("Horizontal")*turn);
            }
            if(Input.GetButton("Vertical"))
            {
                rigidBody.AddTorque(Vector3.right*Input.GetAxis("Vertical")*turn);
            }
            if(Input.GetButtonDown("Jump"))
            {
                rigidBody.AddForce(Vector3.up*200);
            }

        }

        //Camera follow player top view
        if(playerCamera != null)
        {
            Vector3 direction = (Vector3.up*2 + Vector3.back)*2;

            RaycastHit hit;

            if(Physics.Linecast(transform.position, transform.position+direction, out hit))
            {
                playerCamera.transform.position = hit.point;
            }
            else
            {
                playerCamera.transform.position = transform.position+direction;
            }
            playerCamera.transform.LookAt(transform.position);
        }

    }

    void OnCollisionEnter(Collision col)
    {
        if(col.gameObject.tag.Equals("Floor"))
        {
            isFloorTouched = true;
        }
    }

    void OnCollisionExit(Collision col)
    {
        if(col.gameObject.tag.Equals("Floor"))
        {
            isFloorTouched = false;
        }
    }






}
