﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyAi : MonoBehaviour
{
    public float lookRadius = 3f;
    
    Transform target = null;
    NavMeshAgent agent;

    void Start()
    {
        
        target = PlayerManager.instance.player.transform;
        target = GameObject.FindGameObjectWithTag("Player").transform;
        agent = GetComponent<NavMeshAgent>();
    }

    public void SetTarget(Transform target)
    {
        this.target = target;
    }

    void Update()
    {
        float distance = Vector3.Distance(target.position, transform.position);

        if(distance <= lookRadius)
        {
            //chase target
            agent.SetDestination(target.position);

            if(distance <= agent.stoppingDistance)
            {
                // do damage
                // face target
                Debug.Log("Enemy Spotted");
                LookAtTarget();
            }
        }
    }

    void LookAtTarget()
    {
        
        Vector3 dir = (target.position - transform.position).normalized;
        Quaternion lookRot = Quaternion.LookRotation(new Vector3(dir.x, 0, dir.y));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRot, Time.deltaTime *5f);
       
        
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, lookRadius);
    }

}
