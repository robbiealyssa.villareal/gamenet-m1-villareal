﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;  //

public class Shooting : MonoBehaviourPunCallbacks
{
    public Camera camera;
    public GameObject hitEffectPrefab;

    [Header("HP Stuff")]
    public float startHealth = 100;
    private float health;
    public Image healthbar;

    private Animator animator;

    public float killCount;

    public bool isDead;

    public float delay = 5f;

    //[Header("Game Options Panel")]
    //public GameObject gameOptionsPanel;


    void Start()
    {
        health = startHealth;
        healthbar.fillAmount = health / startHealth;

        killCount = 0;
        isDead = false;

        animator = this.GetComponent<Animator>();
    }

    void Update()
    {
       
    }

    public void Fire() 
    {
        RaycastHit hit;
        Ray ray = camera.ViewportPointToRay(new Vector3(0.5f, 0.5f));
        
        if(Physics.Raycast(ray, out hit, 200))
        {
            Debug.Log(hit.collider.gameObject.name);
            
            photonView.RPC("CreateHitEffects", RpcTarget.All, hit.point); // when player fires, all players can see direction 

            if(hit.collider.gameObject.CompareTag("Player") && !hit.collider.gameObject.GetComponent<PhotonView>().IsMine)
            {
                hit.collider.gameObject.GetComponent<PhotonView>().RPC("TakeDamage", RpcTarget.AllBuffered, 10);
            }

        }
    }

    [PunRPC]
    public void TakeDamage(int damage, PhotonMessageInfo info)
    {
        this.health -= damage;
        this.healthbar.fillAmount = health / startHealth;

        if(health <= 0 && !isDead)
        {
            health = 0; 

            isDead = true;

            // UI who killed who    
            Debug.Log(info.Sender.NickName + " killed " + info.photonView.Owner.NickName); 
            GameObject whoKilledText = GameObject.Find("Who Killed Text");
            whoKilledText.GetComponent<Text>().text = info.Sender.NickName + " killed " + info.photonView.Owner.NickName;

            //Die();
            photonView.RPC("Die",RpcTarget.AllBuffered);

            // update killCount
            killCount += 1;
            Debug.Log("Kill COUNT: " + killCount);
            GameObject killCountText = GameObject.Find("Kill Count Text");
            killCountText.GetComponent<Text>().text = info.Sender.NickName + " Kill Count: " + killCount;

        }
        
    }

    [PunRPC]
    public void CreateHitEffects(Vector3 position)
    {
        GameObject hitEffectGameObject = Instantiate(hitEffectPrefab, position, Quaternion.identity);
        Destroy(hitEffectGameObject, .2f);
    }

    [PunRPC]
    public void Die(PhotonMessageInfo info) 
    {
        if(photonView.IsMine)
        {
            
            animator.SetBool("isDead", true);
            StartCoroutine(RespawnCountdown());
        }

        if(killCount >= 3) 
        {
            Debug.Log(info.Sender.NickName + " Won!");
            StartCoroutine(ShowWhoWon(info, delay));

        }

    }

    IEnumerator RespawnCountdown()
    {

        GameObject respawnText = GameObject.Find("Respawn Text");
        float respawnTime = 5.0f;

        while(respawnTime > 0)
        {
            yield return new WaitForSeconds(1.0f);
            respawnTime--;

            transform.GetComponent<PlayerMovementController>().enabled = false;
            respawnText.GetComponent<Text>().text = "You are killed. Respawn in " + respawnTime.ToString(".00");
        }

        animator.SetBool("isDead", false);
        respawnText.GetComponent<Text>().text = "";

        //int randomPointX = Random.Range(-20,20);
        //int randomPointZ = Random.Range(-20,20);
        int rndPosx = Random.Range(0, GameManager.gameManager.spawnPoints.Length); 
        int rndPosz = Random.Range(0, GameManager.gameManager.spawnPoints.Length);


        this.transform.position = GameManager.gameManager.spawnPoints[rndPosx].transform.position;
        this.transform.rotation = GameManager.gameManager.spawnPoints[rndPosz].transform.rotation;
        
        //this.transform.position = new Vector3(randomPointX, 0, randomPointZ);
        //
        
        transform.GetComponent<PlayerMovementController>().enabled = true;

        photonView.RPC("RegainHealth", RpcTarget.AllBuffered);
    }

    [PunRPC]
    public void RegainHealth()
    {
        isDead = false;
        health = 100;
        healthbar.fillAmount = health / startHealth;
    }

    IEnumerator ShowWhoWon(PhotonMessageInfo info, float delay)
    {
        
        GameObject showWhoWonText = GameObject.Find("Show Who Won Text");
        showWhoWonText.GetComponent<Text>().text = info.Sender.NickName + " Won!";

        yield return new WaitForSeconds(delay);

        PhotonNetwork.LoadLevel("LobbyScene");
    }

}
