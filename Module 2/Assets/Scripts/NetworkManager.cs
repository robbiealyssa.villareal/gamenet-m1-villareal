using System.Collections;
using System.Collections.Generic; // Dictionary
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;

public class NetworkManager : MonoBehaviourPunCallbacks
{
    
    [Header("Conenction Status Panel")]
    public Text connectionStatusText;

    [Header("Login UI Panel")]
    public InputField playerNameInput;
    public GameObject loginUiPanel;

    [Header("Game Options Panel")]
    public GameObject gameOptionsPanel;

    [Header("Create Room Panel")]
    public InputField roomNameInputField;
    public InputField playerCountInputField;
    public GameObject createRoomPanel;

    [Header("Join Random Room Panel")]
    public GameObject joinRandomRoomPanel;

    [Header("Show Room List Panel")]
    public GameObject showRoomListPanel;

    [Header("Inside Room Panel")]
    public GameObject insideRoomPanel;
    public Text roomInfotext;

    public GameObject playerListItemPrefab;  //
    public GameObject playerListViewParent;

    public GameObject startGameButton;

    [Header("Room Lsit Panel")]
    public GameObject roomListPanel;
    public GameObject roomItemPrefab; //
    public GameObject roomListParent;

    // cache to display inside roomlist panel
    private Dictionary<string, RoomInfo> cachedRoomList;
    private Dictionary<string, GameObject> roomListGameObjects;
    private Dictionary<int, GameObject> playerListGameObjects;


    #region Unity Functions
    void Start()
    {
        cachedRoomList = new Dictionary<string, RoomInfo>();
        roomListGameObjects = new Dictionary<string, GameObject>();
        ActivatePanel(loginUiPanel);

        PhotonNetwork.AutomaticallySyncScene = true;
    }

    void Update()
    {
        connectionStatusText.text = "Connection status: " + PhotonNetwork.NetworkClientState; // info on connected photon servers
    }

    #endregion

    #region UI Callbacks    
    
    //Onclick listeners
    // Function for login button
    public void OnLoginButtonClicked() 
    {
        
        // verify: playerName is valid
        string playerName = playerNameInput.text;

        if(string.IsNullOrEmpty(playerName)) {
            
            Debug.Log("Player name is invalid");

        } else {
            
            //assign name to local player
            PhotonNetwork.LocalPlayer.NickName = playerName;
            PhotonNetwork.ConnectUsingSettings();
        }
    }

    public void OnCreateRoomButtonClicked() 
    {
         
         // check if theres text 
         string roomName = roomNameInputField.text;

         if(string.IsNullOrEmpty(roomName)) {
             roomName = "Room " + Random.Range(1000, 10000);
         }

        RoomOptions roomOptions = new RoomOptions();
        roomOptions.MaxPlayers = (byte)int.Parse(playerCountInputField.text);

        PhotonNetwork.CreateRoom(roomName, roomOptions);
    }

    public void OnCancelButtonClicked()
    {
        ActivatePanel(gameOptionsPanel);
    }

    public void OnShowRoomListButtonClicked()
    {
        // Check if inside Lobby
        if(!PhotonNetwork.InLobby)
        {
            PhotonNetwork.JoinLobby();
        }

        ActivatePanel(showRoomListPanel);
    }

    public void OnBackButtonClicked()
    {
        if(PhotonNetwork.InLobby)
        {
            PhotonNetwork.LeaveLobby();
        }
        ActivatePanel(gameOptionsPanel);
    }

    public void OnLeaveGameButtonClicked()
    {
        PhotonNetwork.LeaveRoom();  
        // OnLeftRoom() will be called
    }
   
    public void OnJoinRandomRoomClicked()
    {
        ActivatePanel(joinRandomRoomPanel);
        PhotonNetwork.JoinRandomRoom();
    }
    
    public void OnStartGameButtonClicked()
    {
        PhotonNetwork.LoadLevel("GameScene");
    }
    
    #endregion

    #region PUN Callbacks
    public override void OnConnected()
    {   // called when connected to internet
        
        Debug.Log("Connected to the internet");
    }

    public override void OnConnectedToMaster()
    {    // called when connected to photon servers
        
        // First: If connected to Photon Servers
        Debug.Log(PhotonNetwork.LocalPlayer.NickName + " has connected to Photon Servers");

        // When successfully login 
        ActivatePanel(gameOptionsPanel);

    }

    public override void OnCreatedRoom()
    {
        Debug.Log(PhotonNetwork.CurrentRoom.Name + " created!");
    }

    // Instantiate playerlist prefab
    public override void OnJoinedRoom() 
    {  // Called when joined the room.
        Debug.Log(PhotonNetwork.LocalPlayer.NickName + " has joined " + PhotonNetwork.CurrentRoom.Name);
        
        // After joining a room activate inside room panel
        ActivatePanel(insideRoomPanel);

        // After activate inside room panel, update room info for ramaining players inside the room 
        roomInfotext.text = "Room Name: " + PhotonNetwork.CurrentRoom.Name + " Current Player Count: " +
                PhotonNetwork.CurrentRoom.PlayerCount + "/" + PhotonNetwork.CurrentRoom.MaxPlayers; 

        if(playerListGameObjects == null)
        {
            playerListGameObjects = new Dictionary<int, GameObject>();
        }
        
        // Once joined room, Instantiate PlayerList Prefab, Display player list: PhotonNetwork.PlayerList = contains list of players inside the room
        foreach(Player player in PhotonNetwork.PlayerList)
        {
            GameObject playerItem = Instantiate(playerListItemPrefab);
            playerItem.transform.SetParent(playerListViewParent.transform);
            playerItem.transform.localScale = Vector3.one;

            playerItem.transform.Find("PlayerNameText").GetComponent<Text>().text = player.NickName;

            // To determine if that specific player is you, use Actor Number, "You" - indicator
            playerItem.transform.Find("PlayerIndicator").gameObject.SetActive(player.ActorNumber == PhotonNetwork.LocalPlayer.ActorNumber);
            
            playerListGameObjects.Add(player.ActorNumber, playerItem);
        }
    } 

    // Instantiate roomlist prefab
    public override void OnRoomListUpdate(List<RoomInfo> roomList)     // When a room is updated inside the lobby this function                                                                             
    {                                                                   // will be called and returns a list of all room info

        // Clear first before updating the room list
        ClearRoomListGameObjects();
        Debug.Log("OnRoomListUpdate");

        // Host
        startGameButton.SetActive(PhotonNetwork.LocalPlayer.IsMasterClient);  
       
        // Cache room list info   ***
        foreach(RoomInfo info in roomList) 
        {
            Debug.Log(info.Name);

            if(!info.IsOpen || !info.IsVisible || info.RemovedFromList)
            {

                if(cachedRoomList.ContainsKey(info.Name))
                {
                    cachedRoomList.Remove(info.Name);  
                }
            
            } 
            else 
            {
                // Update existing room 
                if(cachedRoomList.ContainsKey(info.Name))
                {
                    cachedRoomList[info.Name] = info;
                }
                else
                {
                    cachedRoomList.Add(info.Name, info);
                }
               

            }
            
        }

        foreach(RoomInfo info1 in cachedRoomList.Values) // Instantiate prefab
        {
            GameObject listItem = Instantiate(roomItemPrefab);
            listItem.transform.SetParent(roomListParent.transform);
            listItem.transform.localScale = Vector3.one;

            listItem.transform.Find("RoomNameText").GetComponent<Text>().text = info1.Name;
            listItem.transform.Find("RoomPlayersText").GetComponent<Text>().text = "Player count: " + info1.PlayerCount
                + " / " + info1.MaxPlayers;
            listItem.transform.Find("JoinRoomButton").GetComponent<Button>().onClick.AddListener(() => OnJoinedRoomClicked(info1.Name));

            // Cache-add GameObjects inside the list
            roomListGameObjects.Add(info1.Name, listItem);
        }

    }

    public override void OnLeftLobby() 
    {  // Clear all gameobj inside room list parent ; Photon override function 
        ClearRoomListGameObjects();

        cachedRoomList.Clear();
    }

    // Determines if new player joins
    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
         // After activate inside room panel, 
         //update room info for ramaining players inside the room 
        roomInfotext.text = "Room Name: " + PhotonNetwork.CurrentRoom.Name + " Current Player Count: " +
                PhotonNetwork.CurrentRoom.PlayerCount + "/" + PhotonNetwork.CurrentRoom.MaxPlayers; 

        GameObject playerItem = Instantiate(playerListItemPrefab);
        playerItem.transform.SetParent(playerListViewParent.transform);
        playerItem.transform.localScale = Vector3.one;

        playerItem.transform.Find("PlayerNameText").GetComponent<Text>().text = newPlayer.NickName;

        // To determine if that specific player is you, use Actor Number, "You" - indicator
        playerItem.transform.Find("PlayerIndicator").gameObject.SetActive(newPlayer.ActorNumber == PhotonNetwork.LocalPlayer.ActorNumber);
            
        playerListGameObjects.Add(newPlayer.ActorNumber, playerItem);
    }

    // If player leaves a room 
    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
         startGameButton.SetActive(PhotonNetwork.LocalPlayer.IsMasterClient);  

         // Update info of the room for remaining players inside the room
        roomInfotext.text = "Room Name: " + PhotonNetwork.CurrentRoom.Name + " Current Player Count: " +
                PhotonNetwork.CurrentRoom.PlayerCount + "/" + PhotonNetwork.CurrentRoom.MaxPlayers; 

        // Destroy prefab 
        Destroy(playerListGameObjects[otherPlayer.ActorNumber]);
        // Remove from inside list view
        playerListGameObjects.Remove(otherPlayer.ActorNumber);
    }

    // Player leaves room, go back to Game Options Menu
    public override void OnLeftRoom()
    {
        // Destroy/Clear gameobj inside the list 
        foreach(var gameobjct in playerListGameObjects.Values)
        {
            Destroy(gameobjct);
        }
        playerListGameObjects.Clear();
        playerListGameObjects = null;
        ActivatePanel(gameOptionsPanel);
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        Debug.LogWarning(message);

        // Creat new 
        string roomName = "Room " + Random.Range(1000,10000);
        RoomOptions roomOptions = new RoomOptions();
        roomOptions.MaxPlayers = 20;

        PhotonNetwork.CreateRoom(roomName, roomOptions);
    }
    #endregion

    #region Private Methods

    private void OnJoinedRoomClicked(string roomName)
    {
        if(PhotonNetwork.InLobby)
        {
            PhotonNetwork.LeaveLobby();
        }
        PhotonNetwork.JoinRoom(roomName);
    }

    private void ClearRoomListGameObjects() // Called before update all roomlist info
    {  // Clear roomlist gameobjects
        foreach(var item in roomListGameObjects.Values)
        {
            Destroy(item);
        }

        roomListGameObjects.Clear();
    }

    #endregion

    # region Public Methods
    
    public void ActivatePanel(GameObject panelToBeActivated) 
    {    // Activate/Diactiavte panels  
        
        loginUiPanel.SetActive(panelToBeActivated.Equals(loginUiPanel));
        gameOptionsPanel.SetActive(panelToBeActivated.Equals(gameOptionsPanel));
        createRoomPanel.SetActive(panelToBeActivated.Equals(createRoomPanel));
        joinRandomRoomPanel.SetActive(panelToBeActivated.Equals(joinRandomRoomPanel));
        showRoomListPanel.SetActive(panelToBeActivated.Equals(showRoomListPanel));
        insideRoomPanel.SetActive(panelToBeActivated.Equals(insideRoomPanel));
        roomListPanel.SetActive(panelToBeActivated.Equals(roomListPanel));

    } 

    #endregion

}
