﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class GameManager : MonoBehaviour
{
    public static GameManager gameManager;
    public Transform[] spawnPoints;
    int numOfPlayers;   //delete

    public GameObject playerPrefab;
    void Start()
    {
        gameManager = this;

        numOfPlayers = PhotonNetwork.PlayerList.Length;  //delete

        if(PhotonNetwork.IsConnectedAndReady)
        {
            //int rndPointX = Random.Range(-10,10);
            //int rndPointZ = Random.Range(-10,10);
            //PhotonNetwork.Instantiate(playerPrefab.name, new Vector3(rndPointX, 0, rndPointZ), Quaternion.identity);
           
            //spawnRndPoint();

            // spawnPoints[].position or use new function [PUNRPC]?
            int rndPos = Random.Range(0, spawnPoints.Length);

            PhotonNetwork.Instantiate(playerPrefab.name, spawnPoints[rndPos].transform.position, spawnPoints[rndPos].transform.rotation, 0);
            
            //PhotonNetwork.Instantiate(playerPrefab.name, spawnPoints[numOfPlayers - 1].transform.position, spawnPoints[numOfPlayers - 1].transform.rotation, 0);
            //hotonNetwork.Instantiate(playerPrefab.name, spawnPoints[numOfPlayers - 1].transform.position, Quaternion.identity);
            
        }
    }

    public void spawnRndPoint()
    {
        // spawnPoints[].position or use new function [PUNRPC]?
        int rndPos = Random.Range(0, spawnPoints.Length);
        //PhotonNetwork.Instantiate(playerPrefab.name, spawnPoints[rndPos].transform.position, Quaternion.identity);
        PhotonNetwork.Instantiate(playerPrefab.name, spawnPoints[rndPos].transform.position, spawnPoints[rndPos].transform.rotation, 0);
        //PhotonNetwork.Instantiate(playerPrefab.name, spawnPoints[numOfPlayers - 1].transform.position, spawnPoints[numOfPlayers - 1].transform.rotation, 0);
        //hotonNetwork.Instantiate(playerPrefab.name, spawnPoints[numOfPlayers - 1].transform.position, Quaternion.identity);
    }

    
    void Update()
    {
        
    }
}
